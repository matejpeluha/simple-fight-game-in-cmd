package game.backend.characters;

import game.backend.characters.atributes.*;
import game.frontend.Visualization;
import org.jetbrains.annotations.NotNull;

abstract public class Character{
    protected float health;
    protected Weapon weapon;
    protected String characterClass;
    protected String name;
    protected double block;
    protected int numOfAttacks;
    protected int cooldown;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getHealth() {
        return health;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public double getBlock() {
        return block;
    }

    public int getNumOfAttacks() {
        return numOfAttacks;
    }

    public int getCooldown() {
        return cooldown;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    protected void counterAttackNecromancer(Character character, double damage){

        if (character instanceof Necromancer){
            takeDamage(this, damage);

            System.out.println("Utokom na nekromancera si si udelil poskodenie " + damage
                + ". Ostava ti( " + this.name + " ) " + this.health + " zivota.");
        }
    }

    protected void takeDamage(Character character, double damage){

        if(character.health <= damage)
            character.health = 0;
        else
            character.health -= damage;
    }

    protected double calculateDamage(Character character){
        return (1 - character.block) * weapon.getDamage();
    }

    public void attack(Character character){
        double damage = 0;

        for(int i = 0; i < numOfAttacks; i++) {

            damage = calculateDamage(character);

            takeDamage(character, damage);

            System.out.println(character.name + " dostal poskodenie " + damage + " " + weapon.getName()
                    + ". Ostava mu ( "  + character.name + " ) " + character.health + " zivota.");

            counterAttackNecromancer(character,damage / 2);

            System.out.println("");
        }

        if(this.cooldown > 0)
            this.cooldown--;

    }

    private boolean revivalNecromancer(Character character){

        if(((Necromancer) character).getRevivals() > 0){
            character.health = 45;
            ((Necromancer) character).setRevivals(0);
            System.out.println("Vyuzita specialna schopnost nekromancera ( " + character.name
                    + " ). Vstal z mrtvych so zivotom 45.");
            return false;
        }

        else{
            System.out.println("Necromancer uz nemoze pouzit specialnu schopnost");
            return true;
        }

    }

    public boolean kill(@NotNull Character character){

        if(character.health <= 0 && character instanceof Necromancer){
            return revivalNecromancer(character);
        }

        else if(character.health <= 0){
            System.out.println();
            System.out.println(this.name + " VYHRAL !");
            return true;
        }

        else
            return false;
    }

    protected boolean isSuperPowerReady(){

        if(this.cooldown > 0){
            System.out.println("Nemozno pouzit. Superscopnost spustitelna o " + this.cooldown + " tahov");
            System.out.println("");
            return false;
        }

        else
            return true;
    }

    public void superPower(Character character){ };


}
