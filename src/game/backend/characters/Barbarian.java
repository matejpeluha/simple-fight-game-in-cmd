package game.backend.characters;

import game.backend.characters.atributes.Axe;
import java.util.*;

public class Barbarian extends Character {

    public Barbarian() {
        this.health = 100;
        this.characterClass = "Barbarian";
        this.name = "";
        this.block = 0.2; //0.1 - 0.2
        this.weapon = new Axe();
        this.numOfAttacks = 1; //1 - 2
        this.cooldown = 5;
    }

    private void updateNumOfAttacks(){
        Random random = new Random();

        int possibility1 = 1;
        int possibility2 = 2;

        this.numOfAttacks = random.nextBoolean() ? possibility1 : possibility2;
    }

    private void updateBlock(){
        this.block = (Math.random() * 0.1 ) + 0.2;
    }

    @Override
    public void attack(Character character){
        updateNumOfAttacks();

        super.attack(character);

        updateBlock();
    }

    @Override
    public void superPower(Character character){

        if( !isSuperPowerReady() )
            return;

        this.weapon = character.weapon;
        this.numOfAttacks = 3;

        System.out.println("Hrac " + this.name + " 3-krat utoci s protihracovou zbranou.");

        super.attack(character);

        this.weapon = new Axe();
    }


}
