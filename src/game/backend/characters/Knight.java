package game.backend.characters;

import game.backend.characters.atributes.Sword;

public class Knight extends Character {

    public Knight() {
        this.health = 120;
        this.characterClass = "Knight";
        this.name = "";
        this.block = 0.2;
        this.weapon = new Sword();
        this.numOfAttacks = 1;
        this.cooldown = 2;
    }

    @Override
    public void attack(Character character){
        this.block = 0.2;
        super.attack(character);
    }

    private void halfAttack(Character character){

        double dmg = calculateDamage(character);

        takeDamage(character, dmg / 2);

        System.out.println(character.name + " dostal poskodenie " + dmg/2 + " " + weapon.getName()
                + ". Ostava mu ( "  + character.name + " ) " + character.health + " zivota.");

        System.out.println("");

    }

    @Override
    public void superPower(Character character){

        if( !isSuperPowerReady() )
            return;

        System.out.println("Aktivovana superschopnost hraca " + this.name + ": 90% blok + polovicny utok(Necromancer bez protitutoku)" );

        this.block = 0.9;

        halfAttack(character);

        this.cooldown = 2;

    }


}
