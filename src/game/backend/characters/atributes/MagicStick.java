package game.backend.characters.atributes;

public class MagicStick extends Weapon {

    public MagicStick() {
        this.lowDamage = 1;
        this.highDamage = 3;
        this.name = "magickou palicou";
    }
}
