package game.backend.characters.atributes;

abstract public class Weapon {

    protected int lowDamage;
    protected int highDamage;
    protected String name; //vysklonovane

    public double getDamage() {
        return (Math.random() * ( this.highDamage - this.lowDamage )) + this.lowDamage;
    }

    public int getLowDamage() {
        return lowDamage;
    }

    public int getHighDamage() {
        return highDamage;
    }

    public String getName(){
        return this.name;
    }

}
