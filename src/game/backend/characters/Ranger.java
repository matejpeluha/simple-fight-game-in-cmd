package game.backend.characters;

import game.backend.characters.atributes.Bow;

public class Ranger extends Character {
    public Ranger() {
        this.health = 90;
        this.characterClass = "Ranger";
        this.name = "";
        this.block = 0;
        this.weapon = new Bow();
        this.numOfAttacks = 3;
        this.cooldown = 3;
    }

    @Override
    public void superPower(Character character){

        if( !isSuperPowerReady() )
            return;

        System.out.println("Aktivovana superschopnost hraca " + this.name + ": 6 sipov" );

        this.numOfAttacks = 9;
        attack(character);

        this.numOfAttacks = 3;
        this.cooldown = 3;
    }

    public String getCharacterClass(){
        return this.characterClass;
    }
}
