package game.backend.characters;

import game.backend.characters.atributes.MagicStick;

public class Necromancer extends Character {

    private int revivals;

    public Necromancer() {
        this.health = 70;
        this.characterClass = "Necromancer";
        this.name = "";
        this.block = -0.10;
        this.weapon = new MagicStick();
        this.numOfAttacks = 2;
        this.revivals = 1;
        this.cooldown = 8;
    }

    private void autoHeal(double damage){
        this.health += damage;
        System.out.println("Utokom si nekromancer ( " + this.name + " ) doplnil zivot na " + this.health);
    }

    @Override
    public void attack(Character character){
        double damage = 0;

        for(int i = 0; i < numOfAttacks; i++) {

            damage = calculateDamage(character);

            takeDamage(character, damage);

            System.out.println(character.name + " dostal poskodenie " + damage + " " + weapon.getName()
                    + ". Ostava mu ( "  + character.name + " ) " + character.health + " zivota.");

            counterAttackNecromancer(character,damage / 2);

            System.out.println("");

            autoHeal(damage);
        }

        if(this.cooldown > 0)
            this.cooldown--;
    }

    public int getRevivals() {
        return revivals;
    }

    public void setRevivals(int revivals){
        this.revivals = revivals;
    }

    @Override
    public void superPower(Character character){

        if( !isSuperPowerReady() )
            return;

        this.health += 20;

        this.cooldown = 6;

        System.out.println("Hrac " + this.name + " pouzil superschopnost a doplnil si zivot na " + this.health);
    }
}
