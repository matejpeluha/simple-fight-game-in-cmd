package game.frontend;

import game.backend.characters.*;
import game.backend.characters.Character;
import game.backend.characters.atributes.Weapon;

import java.util.*;

public class Visualization {

    private Character player1;
    private Character player2;

    private boolean classNameIsValid(String className){
        className = className.toLowerCase();

        return className.equals("knight") || className.equals("barbarian") || className.equals("ranger") || className.equals("necromancer");
    }

    private String writeClassName(){
        Scanner scan = new Scanner(System.in);
        String className = "";

        while( !classNameIsValid(className) ) {
            System.out.println("");
            System.out.println("Zvol si jednu z class:");
            System.out.println("KNIGHT");
            System.out.println("RANGER");
            System.out.println("BARBARIAN");
            System.out.println("NECROMANCER");

            System.out.println("");
            System.out.println("Tvoja volba: ");
            className = scan.nextLine();
            CLS.cls();
        }

        return className.toLowerCase();
    }

    private boolean isNotNameValid(String name){

        if( name.length() <= 0 || name.length() > 20 ){
            System.out.println("Zla velkost mena. Povoleny rozsah 1 - 20 znakov.");
            return true;
        }
        else
            return false;
    }

    private String writeName(){
        Scanner scan = new Scanner(System.in);
        String name = "";

        do {
            System.out.println("Tvoj nickname: ");
            name = scan.nextLine();
            CLS.cls();
        }while( isNotNameValid(name) );

        return name;
    }

    private Character getCharacter(String className) {

        switch (className) {
            case "knight":
                return new Knight();
            case "barbarian":
                return new Barbarian();
            case "ranger":
                return new Ranger();
            default:
                return new Necromancer();
        }
    }

    private void initializePlayer(String className, String playerName){

        if(this.player1 == null){
            this.player1 = getCharacter(className);
            this.player1.setName(playerName);
        }
        else {
            this.player2 = getCharacter(className);
            this.player2.setName(playerName);
        }
    }

    private void setPlayer(){
        Scanner scan = new Scanner(System.in);

        String className = writeClassName();

        String name = writeName();

        initializePlayer(className, name);
    }

    public void startGame(){

        CLS.cls();

        for(int i = 0; i < 2; i++ ) {
            setPlayer();
            CLS.cls();
        }

    }

    public void showStats(){
        Weapon weapon1 = player1.getWeapon();
        Weapon weapon2 = player2.getWeapon();

        System.out.println("");
        System.out.printf("%-10s %-20s %-10s\n", "Meno:", player1.getName(), player2.getName());
        System.out.printf("%-10s %-20s %-10s\n", "Hrdina:", player1.getCharacterClass(), player2.getCharacterClass());
        System.out.printf("%-10s %-20s %-10s\n", "Zdravie:", player1.getHealth(), player2.getHealth());
        System.out.printf("%-10s %-20s %-10s\n", "Blok:", player1.getBlock(), player2.getBlock());
        System.out.printf("%-10s %-20s %-10s\n", "Utok:", weapon1.getLowDamage() + "-" + weapon1.getHighDamage()
                , weapon2.getLowDamage() + "-" + weapon2.getHighDamage());
        System.out.printf("%-10s %-20s %-10s\n", "Cooldown", player1.getCooldown(), player2.getCooldown());
        System.out.println("\n*Cooldown je pocet tahov ktore treba uskutocnit kym mozes pouzit superschopnost.");
    }


    private String writeAttackType(Character attacker){
        Scanner scan = new Scanner(System.in);
        String attackType = "";

        while( !attackType.equals("s") && !attackType.equals("z") ) {
            System.out.println("");
            System.out.println(attacker.getName() + " zvol si typ utoku (zakladny alebo specialny).");
            System.out.println("Na specialny utok ti chyba " + attacker.getCooldown() + " tahov.");
            System.out.println("Tvoja volba (z alebo s): ");
            attackType = scan.nextLine().toLowerCase();
        }

        return attackType;
    }

    private void doAttack(Character attacker, Character defender, String attackType){

        if ( attackType.equals("z") )
            attacker.attack(defender);
        else if( attackType.equals("s") && attacker.getCooldown() <= 0 ){
            attacker.superPower(defender);
        }
        else if( attackType.equals("s") && attacker.getCooldown() > 0 ){
            showStats();
            System.out.println("");
            System.out.println("Nemozno pouzit specialnu schopnost.");
            chooseMove(attacker, defender);
        }
    }

    private void chooseMove(Character attacker, Character defender){
        Scanner scan = new Scanner(System.in);
        String attackType = writeAttackType(attacker);
        CLS.cls();
        doAttack(attacker, defender, attackType);
    }

    public void playGame(){
        Scanner scan = new Scanner(System.in);

        showStats();

        while( true ){

            chooseMove(this.player1, this.player2);

            showStats();

            if( this.player1.kill(this.player2) )
                break;

            chooseMove(this.player2, this.player1);

            showStats();

            if( this.player2.kill(this.player1) )
                break;

        }


    }
}
