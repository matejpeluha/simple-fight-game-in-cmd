package game.frontend;

import java.io.IOException;

public class CLS {

    public final static void cls()
    {
        try
        {
            new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
        }
        catch (Exception E)
        {
            System.out.println(E);
        }
    }
}
