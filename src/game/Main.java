package game;

import game.frontend.Visualization;

public class Main {

    public static void main(String[] args){

        Visualization game = new Visualization();
        game.startGame();
        game.playGame();
    }

}
